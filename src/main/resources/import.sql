/* Populate tabla clientes */

INSERT INTO regiones (id, nombre) VALUES(1, 'Sudamérica');
INSERT INTO regiones (id, nombre) VALUES(2, 'Centroamérica');
INSERT INTO regiones (id, nombre) VALUES(3, 'Norteamérica');
INSERT INTO regiones (id, nombre) VALUES(4, 'Europa');
INSERT INTO regiones (id, nombre) VALUES(5, 'Asia');
INSERT INTO regiones (id, nombre) VALUES(6, 'Africa');
INSERT INTO regiones (id, nombre) VALUES(7, 'Oceanía');
INSERT INTO regiones (id, nombre) VALUES(8, 'Antártida');

INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(1, 'Andrés', 'Guzmán','2018-01-01', 'profesor@bolsadeideas.com', '2018-01-01');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(5, 'Mr. John', 'Doe', '2018-01-02', 'john.doe@gmail.com', '2018-01-02');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(5, 'Linus', 'Torvalds', '2018-01-03', 'linus.torvalds@gmail.com', '2018-01-03');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(4, 'Rasmus', 'Lerdorf', '2018-01-04', 'rasmus.lerdorf@gmail.com', '2018-01-04');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(6, 'Erich', 'Gamma', '2018-02-01', 'erich.gamma@gmail.com', '2018-02-01');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(1, 'Richard', 'Helm', '2018-02-10', 'richard.helm@gmail.com', '2018-02-10');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(2, 'Ralph', 'Johnson', '2018-02-18', 'ralph.johnson@gmail.com', '2018-02-18');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(3, 'John', 'Vlissides', '2018-02-28', 'john.vlissides@gmail.com', '2018-02-28');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(8, 'Dr. James', 'Gosling', '2018-03-03', 'james.gosling@gmail.com', '2018-03-03');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(7, 'Magma', 'Lee', '2018-03-04', 'magma.lee@gmail.com', '2018-03-04');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(3, 'Tornado', 'Roe', '2018-03-05', 'tornado.roe@gmail.com', '2018-03-05');
INSERT INTO cliente (region_id, nombre, apellido, fecha_nacimiento, email, create_at) VALUES(1, 'Jade', 'Doe', '2018-03-06', 'jane.doe@gmail.com', '2018-03-06');

/* Crear usuarios y roles */
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('renato', '$2a$10$FXGJvfo1oiOk3zPDsE22OuRxftjE8NuuQ0YZmditv91U5wioKVcde', 1, 'renato', 'quiroz', 'renatoquiroz@gmail.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin', '$2a$10$D3dSnFTZP4rulW0hjgbTUuq8W.PdMI8hQOHB7g77B.HzNOna5h8Nq', 1, 'juan', 'juan', 'juan@gmail.com');

INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);
