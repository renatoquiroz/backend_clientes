package com.renato.springboot.backend.apirest.models.repository;

import com.renato.springboot.backend.apirest.models.entity.Cliente;
import com.renato.springboot.backend.apirest.models.entity.Region;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    @Query("from Region")
    public List<Region> findAllRegiones();

}
