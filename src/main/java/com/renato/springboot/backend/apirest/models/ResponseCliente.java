package com.renato.springboot.backend.apirest.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ResponseCliente {

    private Long id;
    private String nombre;

}
