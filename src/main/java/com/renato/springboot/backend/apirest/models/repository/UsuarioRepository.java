package com.renato.springboot.backend.apirest.models.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.renato.springboot.backend.apirest.models.entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    public Usuario findByUsername(String username);

    // busqueda por query con dos parametros

    // @Query("select u from Usuario u where u.username=?1 and u.email=?2")
    // public Usuario findByUsername2(String username, String email);

}
