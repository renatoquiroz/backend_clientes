package com.renato.springboot.backend.apirest.auth;

public class JwtConfig {
    public static final String LLAVE_SECRETA = "clave.secreta";

    public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\r\n" +
            "MIIEowIBAAKCAQEAwRRkppjNSOZkENkh8/JNk7vjVq1J7jRWT89ZrgOYbS7BkKDX\r\n" +
            "YYo3ylxi/Z7aQE5SYP1RIuz0Iw8ZuCsPr4Sh4nbkbWIPnthTB0ZWizmQCC2La81H\r\n" +
            "U78zMCw0AetdGVU0+P0LAh+9lZAdXiZiTqFviNbOj/pfJqyPz6r9/7/HUDVuxpeO\r\n" +
            "KXQO7PMhsQrwCAkj7oxVdXyoKjByyx1vz19A9sZgKrWhthhg409ngy3/RRTtzXqf\r\n" +
            "gg6RgUNIeKmhQjC4E+1K8JtG3dBCFpGuZaCc/Y+PO4aj7c2JaQ2BtQvw9ZmlQmIA\r\n" +
            "RlrD2wTfkld/gnsah9KPAUGzSK2Am2jiTTVnXwIDAQABAoIBAFYkOg+VxqjKmURn\r\n" +
            "C13h8biCsBfAsmZDFWsAEHuxgPTdUmCrUcxjtSZkd4m9sJPWHazF98gEPZvSpd/j\r\n" +
            "3lipbOwzrRAcGun8i3aIbB4rbVYos7ZB3JvBhx3r6rwcfOnLeRnJE3s8HAI5TNDv\r\n" +
            "gRahsbg0Ve4ofwErJfI50J6kulDO75w2yAMefthdLDy9wqtCTwE/elcmZ3318GB+\r\n" +
            "ctJeuemkDoyLTbaDrh98erewpz2WRCxXqTV9mDVzfsL/4Vqky9iCcF4ZhMYkhk22\r\n" +
            "26UVfHUAd0Ovfe5KMUzGRpH0gc6FdSrkGfsWV0gp9anm2cwFeTKiVJmv2eo93yEn\r\n" +
            "MySolpkCgYEA4IZIzyTSZ2pwRkM8QITjwYN7KsQlcu3kCxnf1h7LIPKWgbr5P6OU\r\n" +
            "t842TYFMODCb/CCoX4Oe9YON+hkP+BDlcMvlmArO296gQV3mtsuMWChkRTmQ0fB9\r\n" +
            "X7ryqpz9ry3i/RKydU+mPyFD5DbEn363gTv+K0OxmX26bItLn3rCgOMCgYEA3CWe\r\n" +
            "HLX33o5x1NHAKCB9c/fObX/60uQwVQocGWyfeDleI8G22PAIL5K3Ns+s0RFcHVDJ\r\n" +
            "IXicZ52ceVCCTpzw1KBX9f6ah9cVxIRV0UelSgC8nj4IivDsUcn8YIai1rNzfPCt\r\n" +
            "5d6iN2SqnP4vDpp8hTOZR5d0j0W2FbVwsFzLtFUCgYEAgLqdLh7Xf7GYE8Di378R\r\n" +
            "clb3HCr/qahZUkAQhQx8vDQ6NMFFvMYGM2hI3CEg2SqNlH4I61JkpjE6CsWp0Tmm\r\n" +
            "wwg0Z/ryZT98NF4pNG751WW7L7F0pdmzmFpwXX/LN1Agz6aTqQz0rUdeTI9WJngZ\r\n" +
            "sD/8V0PlpLJgW3F190cEE68CgYAj3YkN2mOAgapv5qAsqWZm30dlNYVymDR7lkMP\r\n" +
            "rU+psYbxwlx8qVZcEcYBiwH3qaFdMU0jQ9gPVXEpnoEsN4tQyLKr5Afe+56TPpAQ\r\n" +
            "oWB/VvFjwm133VpS1NpmC2k6G1BEWZ2rJoM9DQxyuUKHWYnR1Z8yN62Ire3FSaML\r\n" +
            "SILzZQKBgFzwPCDsrBbNi2I0zzSuPptinosyEMwp4Fx/V0JpOMcB37k2wEbpN7v5\r\n" +
            "LbwCiTtzS6oxWfY2yOA6eICTX2Eiu5a5MP/uShsBCKRR2dGuqxD3K3TmiWielY34\r\n" +
            "+E+YwC7OKZiSJ97cePVhMbTt5RrIkXSuQbjWAdZkC5BAUJEWTpvl\r\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\r\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwRRkppjNSOZkENkh8/JN\r\n" +
            "k7vjVq1J7jRWT89ZrgOYbS7BkKDXYYo3ylxi/Z7aQE5SYP1RIuz0Iw8ZuCsPr4Sh\r\n" +
            "4nbkbWIPnthTB0ZWizmQCC2La81HU78zMCw0AetdGVU0+P0LAh+9lZAdXiZiTqFv\r\n" +
            "iNbOj/pfJqyPz6r9/7/HUDVuxpeOKXQO7PMhsQrwCAkj7oxVdXyoKjByyx1vz19A\r\n" +
            "9sZgKrWhthhg409ngy3/RRTtzXqfgg6RgUNIeKmhQjC4E+1K8JtG3dBCFpGuZaCc\r\n" +
            "/Y+PO4aj7c2JaQ2BtQvw9ZmlQmIARlrD2wTfkld/gnsah9KPAUGzSK2Am2jiTTVn\r\n" +
            "XwIDAQAB\r\n" +
            "-----END PUBLIC KEY-----";

    public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\r\n" +
            "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCN//pbur8sUnw4\r\n" +
            "rhJQ+F8l9rK/JmtH8MeGyP1wm9h5s6giDfRbgBDkp0OyYSDmddJ9yFkaY7ZIjhcj\r\n" +
            "JYjvZFwxS7UX8iwUfutQVjtXe+7ZOKxalp/yrvqzd40AnuxCXWkKHLjlwnpQj38g\r\n" +
            "7Fc0YOaZXg0+TeT8lKHGLTLHVleqlvJlJ5bd+9sw2TgKIs2L3mjpe66Mco++aXUF\r\n" +
            "dJWzdFHglCAtW7g2vUUxlcKufR1sd+REg7aLGXvyhAHYBFIuJWpQFS69NoCkGkq1\r\n" +
            "0dGxa3n0TlRXnRGTtgxn4xhb/m3QW6lFO+NITuGFJ61yF/uOKXQbSyrd7ukPtWcR\r\n" +
            "h9rIv89/AgMBAAECggEAA2nlc2IUaXTcLqKkpxSpeOiq2GSMjLCxo0Bes1/Tvu0+\r\n" +
            "O/9Y4gA8p7qLqpSiLVgkxMhVuAgwoCLmVH55AcRIYRkAVACK5b09W/OOvqYokklF\r\n" +
            "/3GCi9uABGN53Mt1Q40qUsdo/IEF3rzsYfa8i/JOaN51dAt8V0QddX2trFQbjGrG\r\n" +
            "Yyg8FAxtmOZnddAvkEhSn6UOBcrVMVnN5S/nactu6SmCV+dwMF5Ws6wRkM93ZVu/\r\n" +
            "uGzSjXFUB3eFVBJc15a1TUCZpTcO4aHKOiaakdwIZeZgWqzRbJs2CHrdmXN0W56e\r\n" +
            "cPSaanYO1lX46n163SPBHDpmQERQBZuzMka1x0QpmQKBgQDABPhLbALFij6GwLef\r\n" +
            "SfK6Z6mXT3HKuFO2kZb4BRFDyp/4kOji9dqDjUTV0HkHGK97ICtU80Ug6G8Wiw2O\r\n" +
            "u9FTfvJx783ttk6fXPtT9rfbxT2v1lHP1IrKEdSnxtfqyf50MpYvsCKwgC5UkMk/\r\n" +
            "3dYwUjtSYG5t1/+wHEWh6HbKOQKBgQC9UGdQmUtHpNERSC0hWEf9e0rKz/86eQu2\r\n" +
            "AS6EycqGzIWpdAZGnsylh5fQC5ay/ojIqB26Blc+jlbClRPc4kZLoclN7yxGcoVA\r\n" +
            "7UxXJPPCRGM5/FnIiv0CNBzjaQax+MzTpi3jMWc1GVDjKeAaPMbqa3eZAvm7VQC8\r\n" +
            "FI2GywJHdwKBgFgV75Epy1uzP1uxrIp18dNxGms/FoRYao6l2wuLx4/BtADi+PKk\r\n" +
            "tckDVUCAkgSfXmCtMTOXJW8jMVK2F1IS9sPGyJFb4KvbpdWVYfXyiAVZRiMTvhQI\r\n" +
            "/GIWK+eLYCFk6EWFrz/RHRi6tIY2pRuXOFqSXoQ7mGzrWXAAiDDt4xCBAoGBALig\r\n" +
            "M0HNXjdOq335L9o3KG4y0WwdkUHLTvp/CIkD6vmveCMcPgqdT+OW6x1AUAXcajWW\r\n" +
            "jvceu/MMCdzWWnquSfP0okfZGwrRINeDFVJffUJWQ5cHXeBQfBKsFAhMIvVqOQeo\r\n" +
            "J1RMl+SW/eWsrxunbi6sBAU7e6WQbpj+Sk/MqDpdAoGADTHFi0tC1GmB8qDmF2ox\r\n" +
            "9Sb5gVDQQgcXPFNP5ZaK721uysshQZSYr6tb9FRwMY9qnc8eHXWqpN5PYXuEFH38\r\n" +
            "0yE6f5oMkI6MYugKwhzQ47Lawkk1BWhD14PJcs3rQ5h/YvaMUaUj6JXa33USg7N3\r\n" +
            "34l6y7IoNaO1Jeuj0K8o94M=\r\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\r\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjf/6W7q/LFJ8OK4SUPhf\r\n" +
            "JfayvyZrR/DHhsj9cJvYebOoIg30W4AQ5KdDsmEg5nXSfchZGmO2SI4XIyWI72Rc\r\n" +
            "MUu1F/IsFH7rUFY7V3vu2TisWpaf8q76s3eNAJ7sQl1pChy45cJ6UI9/IOxXNGDm\r\n" +
            "mV4NPk3k/JShxi0yx1ZXqpbyZSeW3fvbMNk4CiLNi95o6XuujHKPvml1BXSVs3RR\r\n" +
            "4JQgLVu4Nr1FMZXCrn0dbHfkRIO2ixl78oQB2ARSLiVqUBUuvTaApBpKtdHRsWt5\r\n" +
            "9E5UV50Rk7YMZ+MYW/5t0FupRTvjSE7hhSetchf7jil0G0sq3e7pD7VnEYfayL/P\r\n" +
            "fwIDAQAB\r\n" +
            "-----END PUBLIC KEY-----";

}
