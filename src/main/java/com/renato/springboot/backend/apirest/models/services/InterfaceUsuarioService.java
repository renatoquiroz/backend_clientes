package com.renato.springboot.backend.apirest.models.services;

import com.renato.springboot.backend.apirest.models.entity.Usuario;

public interface InterfaceUsuarioService {

    public Usuario findByUsername(String username);

}
